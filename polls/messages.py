from django.http import Http404
from django.template import loader
from .models import Message
from django.shortcuts import render_to_response


def get(request):
    try:
        chat_set = Message.objects.order_by("-created")
        chat_set = Message.objects.reverse()
    except Message.DoesNotExist:
        raise Http404("Has no messages")

    return render_to_response('polls/messages.html', {'chat_set': chat_set})
