from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, Http404, HttpResponsePermanentRedirect
from .models import Message
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.forms import modelformset_factory
from django.template import loader
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.db import IntegrityError

@login_required
def index(request):
    form = {}

    if request.POST:
        form['text'] = request.POST.get('text')
        form['user'] = request.user
        m = Message.objects.create(user=form['user'], message=form['text'])
        m.save()
    try:
        chat_set = Message.objects.order_by("-created")
        chat_set = Message.objects.reverse()
    except Message.DoesNotExist:
        raise Http404("Has no messages")
    template = loader.get_template('polls/latest.html')
    context = {
        'chat_set': chat_set,
    }
    return HttpResponse(template.render(context, request))

def add_new_message(request):
    form = {}

    if request.POST:
        form['text'] = request.POST.get('text')
        form['user'] = request.user
        m = Message.objects.create(user=form['user'], message=form['text'])
        m.save()



def login_join(request):
    errors = []
    form = {}

    if request.POST:
        form['name'] = request.POST.get('name')
        form['password'] = request.POST.get('password')

        if not form['name']:
            errors.append('Заполните имя')
        if not form['password']:
            errors.append('Введите пароль')
        if not errors and form:
            # user = User.objects.create_user(username=form['name'], password=form['password'], email=form['email'])
            user = authenticate(username=form['name'], password=form['password'])

            if user is not None:
                login(request, user)
            else:
                errors.append('Incorrect input')

        return redirect(reverse('index'))
    return render(request, 'polls/login.html', {'errors': errors, 'form': form})


def signup(request):
    try:
        errors = []
        form = {}

        if request.POST:
            form['name'] = request.POST.get('name')
            form['email'] = request.POST.get('email')
            form['password'] = request.POST.get('password')
            form['c_password'] = request.POST.get('c_password')

            if not form['name']:
                errors.append('Input name')
            if '@' not in form['email']:
                errors.append('Incorrect email')
            if not form['password']:
                errors.append('Input a password')
            if form['password'] != form['c_password']:
                errors.append('Passwords must match')
            if not errors and form:
                user = User.objects.create_user(username=form['name'], password=form['password'], email=form['email'])
                user = authenticate(username=form['name'], password=form['password'])
            if user is not None:
                login(request, user)
                return redirect(reverse(index))
        return render(request, 'polls/signup.html', {'errors': errors, 'form': form})

    except IntegrityError:
        errors.append('User already exists')





'''
def logout(request):
    template_response = views.logout(request)
    messages.success(request, 'You have been logged out')
    return redirect(reverse('polls:login'))
'''
