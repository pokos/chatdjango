from django.conf.urls import url, include
from . import views, messages, new_message
from django.contrib.auth.views import logout

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', views.login_join, name='login_join'),
    url(r'^logout/$', logout, kwargs={'next_page': '/polls/login'}, name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^messages/$', messages.get, name='messages'),
    url(r'^new_message/$', new_message.get, name='new_message'),
]